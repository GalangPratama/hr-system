<div class="main-menu menu-fixed menu-light menu-accordion    menu-shadow " data-scroll-to-active="true">
  <div class="main-menu-content">
    <ul class="navigation navigation-main" id="main-menu-navigation" data-menu="menu-navigation">
        <li class="nav-item"><a href="{{ url('/') }}"><i class="la la-home"></i><span class="menu-title" data-i18n="nav.dash.main">Dashboard  Activities</span></a>
            <ul class="menu-content">
              <li class="{{ Request::is('/') ? 'active' : null }}">
                <a class="menu-item" href="{{ url('/') }}" data-i18n="nav.dash.ecommerce">HOME</a>
              </li>
              <li class="{{ Request::is('absen') ? 'active' : null }}">
                <a class="menu-item" href="{{ url('inven') }}" data-i18n="nav.dash.crypto">INVENTORY</a>
              </li>
            </ul>
        </li>

        {{-- <li class="nav-item"><a href="{{ url('#') }}"><i class="la la-safari"></i><span class="menu-title" data-i18n="nav.dash.main">HR Parameters</span></a>
          <ul class="menu-content">
            <li class="{{ Request::is('#') ? 'active' : null }}">
              <a class="menu-item" href="{{ url('#') }}" data-i18n="nav.dash.ecommerce">Employee</a>
            </li>
            <li class="{{ Request::is('#') ? 'active' : null }}">
              <a class="menu-item" href="{{ url('#') }}" data-i18n="nav.dash.crypto">Division</a>
            </li>
            <li class="{{ Request::is('#') ? 'active' : null }}">
              <a class="menu-item" href="{{ url('#') }}" data-i18n="nav.dash.crypto">Departement</a>
            </li>
            <li class="{{ Request::is('#') ? 'active' : null }}">
              <a class="menu-item" href="{{ url('#') }}" data-i18n="nav.dash.crypto">Project</a>
            </li>
            <li class="{{ Request::is('#') ? 'active' : null }}">
              <a class="menu-item" href="{{ url('#') }}" data-i18n="nav.dash.crypto">Libur</a>
            </li>
            <li class="{{ Request::is('#') ? 'active' : null }}">
              <a class="menu-item" href="{{ url('#') }}" data-i18n="nav.dash.crypto">User Access</a>
            </li>
          </ul>
        </li>

        <li class="nav-item"><a href="{{ url('#') }}"><i class="la la-edit"></i><span class="menu-title" data-i18n="nav.dash.main">Reporting</span></a>
          <ul class="menu-content">
            <li class="{{ Request::is('#') ? 'active' : null }}">
              <a class="menu-item" href="{{ url('#') }}" data-i18n="nav.dash.ecommerce">Monitor Absen</a>
            </li>
            <li class="{{ Request::is('#') ? 'active' : null }}">
              <a class="menu-item" href="{{ url('#') }}" data-i18n="nav.dash.crypto">Rekap Absen</a>
            </li>
            <li class="{{ Request::is('#') ? 'active' : null }}">
              <a class="menu-item" href="{{ url('#') }}" data-i18n="nav.dash.crypto">Cetak Temporary ID Card</a>
            </li>
          </ul>
        </li> --}}
        
        <li>
          <a class="dropdown-item" href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                     document.getElementById('logout-form').submit();"><i class="la la-sign-out"></i><span class="menu-title" data-i18n="nav.dash.main">Logout</span></a>
        
           <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
          @csrf
          </form>
        </li>
      </ul>
    </div>
  </div>