<!-- CSS -->
<!DOCTYPE html>
<html class="loading" lang="en" data-textdirection="ltr">
<head>
    <!-- CSRF Token -->
  <meta name="csrf-token" content="{{ csrf_token() }}">
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
  <meta name="description" content="Modern admin is super flexible, powerful, clean &amp; modern responsive bootstrap 4 admin template with unlimited possibilities with bitcoin dashboard.">
  <meta name="keywords" content="admin template, modern admin template, dashboard template, flat admin template, responsive admin template, web app, crypto dashboard, bitcoin dashboard">
  <meta name="author" content="PIXINVENT">
  <title>Total Inti Corpora
  </title>
  <link rel="apple-touch-icon" href="{{url('app-assets/images/logo/logo-tora.png')}}">
  <link rel="shortcut icon" type="image/x-icon" href="{{url('app-assets/images/logo/logo-tora.png')}}">
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Quicksand:300,400,500,700"
  rel="stylesheet">
  <link href="https://maxcdn.icons8.com/fonts/line-awesome/1.1/css/line-awesome.min.css"
  rel="stylesheet">
  <!-- BEGIN VENDOR CSS-->
  <link rel="stylesheet" type="text/css" href="{{url('app-assets/css/vendors.css')}}">
  <!-- END VENDOR CSS-->
  <!-- BEGIN MODERN CSS-->
  <link rel="stylesheet" type="text/css" href="{{url('app-assets/css/app.css')}}">
  <!-- END MODERN CSS-->
  <!-- BEGIN Page Level CSS-->
  <link rel="stylesheet" type="text/css" href="{{url('app-assets/css/core/menu/menu-types/vertical-menu.css')}}">
  <link rel="stylesheet" type="text/css" href="{{url('app-assets/css/core/colors/palette-gradient.css')}}">
  <!-- END Page Level CSS-->
  <!-- BEGIN Custom CSS-->
  @yield('css')
  <link rel="stylesheet" type="text/css" href="{{url('assets/css/style.css')}}">
  <!-- END Custom CSS-->
</head>
<!-- End CSS -->

<body class="vertical-layout vertical-menu 2-columns   menu-expanded fixed-navbar"
data-open="click" data-menu="vertical-menu" data-col="2-columns">
  <!-- Header-->
  @include('master.header')
  <!-- End HEADER -->

  <!-- SIDEBAR-->
    @include('master.sidebar')
  <!-- End SIDEBAR -->

  <!-- CONTENT -->
  <div class="app-content content">
    <div class="content-wrapper">
      <div class="content-header row">
      </div>
      <div class="content-body">

        @yield('content')

      </div>
    </div>
</div>
  <!-- End CONTENT -->

  <!-- FOOTER-->
  @include('master.footer')
  <!-- End FOOTER -->

  <!-- BEGIN VENDOR JS-->
  <script src="{{url('app-assets/vendors/js/vendors.min.js')}}" type="text/javascript"></script>
  <!-- BEGIN MODERN JS-->
  <script src="{{url('app-assets/js/core/app-menu.js')}}" type="text/javascript"></script>
  <script src="{{url('app-assets/js/core/app.js')}}" type="text/javascript"></script>
  <script src="{{url('app-assets/js/scripts/customizer.js')}}" type="text/javascript"></script>
  <!-- END MODERN JS-->
  <!-- EXTERNAL JS-->
  <script type="text/javascript" src="{{url('app-assets/js/scripts/ui/compact-menu.js')}}"></script>
    @yield('js')
  <!-- END EXTERNAL JS-->
</body>
<div id="confirmModal" class="modal fade" role="dialog">
  <div class="modal-dialog" id="modal-delete">
      <div class="modal-content">
          <div class="modal-header" style="text-align:center; display:block">
              <h2 class="modal-title">Konfirmasi</h2>
          </div>
          <div class="modal-body">
              <h4 style="margin:0; text-align:center;">Anda yakin ingin menghapus data ini ?</h4>
          </div>
          <div class="modal-footer">
              <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
              <button type="button" name="konfirmasi" id="konfirmasi" class="btn btn-danger">OK</button>
          </div>
      </div>
  </div>
</div>

<div class="modal fade" id="tesModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
              <div class="left-mdl"  style="width: 100%">
                  <h4>User &nbsp: <b><span id="ivnUser"></span></b></h4>
                  <h4>Divisi : <b><span id="ivnDivisi"></span></b></h4>
              </div>
              <div class="right-mdl" style="width: 100%; float: right;">
                <h4>Tanggal : <b><span id="ivnDate"></span></b></h4>
              </div>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
          </button>
      </div>
      <div class="modal-body">
          <table id="mdl_gambar" class="table table-striped">
              <thead style="width: 100%;">
                  <tr>
                      <th class="text-center">Serial Number</th>
                      <th class="text-center">Merk / Model</th>
                      <th class="text-center">Processor</th>
                      <th class="text-center">RAM</th>
                      <th class="text-center">Storage</th>
                      <th class="text-center">OS</th>
                      <th class="text-center">Tanggal Handover</th>
                      <th class="text-center">Keterangan</th>
                  </tr>
              </thead>
              <tbody>
                  <tr>
                      <td class="text-center" id="ivnSn"> </td>
                      <td class="text-center" id="ivnMerk"> </td>
                      <td class="text-center" id="ivnProcessor"> </td>
                      <td class="text-center" id="ivnRam"> </td>
                      <td class="text-center" id="ivnStorage"> </td>
                      <td class="text-center" id="ivnOs"> </td>
                      <td class="text-center" id="ivnTanggal"> </td>
                      <td class="text-center" id="ivnKeterangan"> </td>
                  </tr>
              </tbody>
          </table>
      </div>
      <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
  </div>
  </div>
</div>


</html> 