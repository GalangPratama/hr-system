@extends('master.master')

@section('css')
<link rel="stylesheet" type="text/css" href="{{ url('app-assets/vendors/css/tables/datatable/datatables.min.css') }}">
<link rel="stylesheet" type="text/css" href="{{ url('app-assets/fonts/simple-line-icons/style.css') }}">
@endsection

@section('content')
    
<div class="row">
  <div class="col-lg-6 col-12">
    <div class="card pull-up">
      <div class="card-content">
        <div class="card-body">
          <div class="media d-flex">
            <div class="media-body text-left">
              <h6 class="text-muted">Total Laptop</h6>
              <h3>50</h3>
            </div>
            <div class="align-self-center">
              <i class="icon-trophy success font-large-2 float-right"></i>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="col-lg-6 col-12">
    <div class="card pull-up">
      <div class="card-content">
        <div class="card-body">
          <div class="media d-flex">
            <div class="media-body text-left">
              <h6 class="text-muted">Total PC</h6>
              <h3>100</h3>
            </div>
            <div class="align-self-center">
              <i class="icon-screen-desktop succes font-large-2 float-right"></i>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="col-lg-6 col-12">
    <div class="card pull-up">
      <div class="card-content">
        <div class="card-body">
          <div class="media d-flex">
            <div class="media-body text-left">
              <h6 class="text-muted">Total CCTV</h6>
              <h3>100</h3>
            </div>
            <div class="align-self-center">
              <i class="icon-camrecorder succes font-large-2 float-right"></i>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="col-lg-6 col-12">
    <div class="card pull-up">
      <div class="card-content">
        <div class="card-body">
          <div class="media d-flex">
            <div class="media-body text-left">
              <h6 class="text-muted">Total GPS</h6>
              <h3>100</h3>
            </div>
            <div class="align-self-center">
              <i class="icon-call-in danger font-large-2 float-right"></i>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection

@section('js')
<script src="{{ url('app-assets/vendors/js/tables/datatable/datatables.min.js') }}" type="text/javascript"></script>
<script src="{{ url('app-assets/js/scripts/tables/datatables-extensions/datatables-sources.js') }}"
  type="text/javascript"></script>
@endsection