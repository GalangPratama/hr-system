@extends('master.master')

@section('css')
<link rel="stylesheet" type="text/css" href="{{ url('app-assets/vendors/css/tables/datatable/datatables.min.css') }}">
<link rel="stylesheet" type="text/css" href="{{ url('https://cdn.datatables.net/buttons/1.6.1/css/buttons.dataTables.min.css') }}">
<style>
@media(min-width: 576px){
  #modal-delete{
    max-width: 700px;
  }
}
</style>
@endsection

@section('content')

<div class="card">
  <div class="card-content">
    <div class="card-body">
      <ul class="nav nav-tabs nav-topline">
        <li class="nav-item">
          <a class="nav-link active" id="base-tab21" data-toggle="tab" aria-controls="tab21"
          href="#tab21" aria-expanded="true">Input Mode</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" id="base-tab22" data-toggle="tab" aria-controls="tab22" href="#tab22"
          aria-expanded="false">Broser Mode</a>
        </li>
      </ul>
      <div class="tab-content px-1 pt-1 border-grey border-lighten-2 border-0-top">
        <div role="tabpanel" class="tab-pane active" id="tab21" aria-expanded="true" aria-labelledby="base-tab21">
          <section id="basic-form-layouts">
            <div class="row match-height">
              <div class="col-md-12">
                <div class="card">
                  <div class="card-header">
                    <h4 class="card-title" id="basic-layout-form">Project Info</h4>
                    <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
                    <div class="heading-elements">
                      <ul class="list-inline mb-0">
                        <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                        <li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
                        <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                      </ul>
                    </div>
                  </div>
                  <div class="card-content collapse show">
                    <div class="card-body">
                      <span id="form_result"></span>
                      <form enctype="multipart/form-data">
                        @csrf  
                          <div class="form-body">
                            <div class="row">
                              <div class="col-md-6">
                                <div class="col-md-12">
                                  <div class="form-group">
                                    <label for="sn">Serial Number</label>
                                    <input type="text" id="ivnSn" class="form-control" placeholder="57302342323" name="ivnSn">
                                  </div>
                                </div>
                                <div class="col-md-12">
                                  <div class="form-group">
                                    <label for="merk">Merk / Model</label>
                                    <input type="text" id="ivnMerk" class="form-control" placeholder="Lenovo" name="ivnMerk">
                                  </div>
                                </div>
                                <div class="col-md-12">
                                  <div class="form-group">
                                    <label for="processor">Processor</label>
                                    <input type="text" id="ivnProcessor" class="form-control" placeholder="Core-I3" name="ivnProcessor">
                                  </div>
                                </div>
                                <div class="col-md-12">
                                  <div class="form-group">
                                    <label for="ram">RAM</label>
                                    <input type="text" id="ivnRam" class="form-control" placeholder="8GB" name="ivnRam">
                                  </div>
                                </div>
                                <div class="col-md-12">
                                  <div class="form-group">
                                    <label for="storage">Storage</label>
                                    <input type="text" id="ivnStorage" class="form-control" placeholder="1TB" name="ivnStorage">
                                  </div>
                                </div>
                                <div class="col-md-12">
                                  <div class="form-group">
                                    <label for="os">Operating System</label>
                                    <input type="text" id="ivnOs" class="form-control" placeholder="Windows" name="ivnOs">
                                  </div>
                                </div>
                              </div>

                              <div class="col">
                                <div class="col-md-12">
                                  <div class="form-group">
                                    <label for="name_karyawan">Nama Karyawan</label>
                                    <input type="text" id="ivnUser" class="form-control" placeholder="Dadang" name="ivnUser">
                                  </div>
                                </div>
                                <div class="col-md-12">
                                  <div class="form-group">
                                    <label for="divisi">Divisi</label>
                                    <input type="text" id="ivnDivisi" class="form-control" placeholder="CS" name="ivnDivisi">
                                  </div>
                                </div>
                                <div class="col-md-12">
                                  <div class="form-group">
                                    <label for="tgl_handover">Tanggal Handover</label>
                                      <input type="date" id="ivnTanggal" class="form-control" name="ivnTanggal" data-toggle="tooltip" data-trigger="hover" data-placement="top" data-title="Tanggal">
                                  </div>
                                </div>
                                <div class="col-md-12">
                                  <div class="form-group">
                                    <label for="ivnKeterangan">Keterangan</label>
                                    <textarea id="ivnKeterangan" rows="6  " class="form-control" name="ivnKeterangan" placeholder="Notes"></textarea>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                          <div class="form-actions">
                            <button type="button" class="btn btn-warning mr-1">
                              <i class="ft-x"></i> Cancel
                            </button>
                            <button type="button" class="btn btn-primary" onclick="saveAset()">
                              <i class="la la-check-square-o"></i> Save
                            </button>
                          </div>
                      </form>
                    </div>
                  </div>
                </div> 
              </div>
            </div>
          </section>
        </div>
        <div class="tab-pane" id="tab22" aria-labelledby="base-tab22">
          <section id="basic-form-layouts">
            <div class="row match-height">
              <div class="col-md-12">
                <div class="card">
                  <div class="card-header">
                    <h4 class="card-title" id="basic-layout-form">Data Inventori</h4>
                    <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
                    <div class="heading-elements">
                      <ul class="list-inline mb-0">
                        <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                        <li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
                        <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                      </ul>
                    </div>
                  </div>
                  <div class="card-content collapse show">
                    <div class="card-body">
                      <table width="100%" id="dataInven" class="table table-striped table-bordered">
                        <thead>
                          <tr class="text-center">
                            <th>Serial Number</th>
                            <th>Merk / Model</th>
                            <th>Processor</th>
                            <th>RAM</th>
                            <th>Storage</th>
                            <th>OS</th>
                            <th>Last User</th>
                            <th>Divisi</th>
                            <th>Tanggal Handover</th>
                            <th>Keterangan</th>
                            <th>Detail</th>
                            <th>Action</th>
                          </tr>
                        </thead>
                        
                      </table>
                    </div>
                  </div>
                </div> 
              </div>
            </div>
          </section>
        </div>
      </div>
    </div>
  </div>
</div>



@endsection

@section('js')
<script src="{{ url('app-assets/vendors/js/tables/datatable/datatables.min.js') }}" type="text/javascript"></script>
<script src="{{ url('app-assets/js/scripts/tables/datatables-extensions/datatables-sources.js') }}" type="text/javascript"></script>
<script src="{{ url('app-assets/js/scripts/tables/datatables-extensions/datetime-moment.js') }}" type="text/javascript"></script>
<script src="{{ url('app-assets/js/scripts/tables/datatables-extensions/moment.min.js') }}" type="text/javascript"></script>
<script src="{{ url('https://cdn.datatables.net/buttons/1.6.1/js/dataTables.buttons.min.js') }}" type="text/javascript"></script>
<script src="{{ url('app-assets/js/scripts/navs/navs.js') }}" type="text/javascript"></script>
<script src="{{url('https://unpkg.com/sweetalert2@7.18.0/dist/sweetalert2.all.js')}}"></script> 
<script>
  $.ajaxSetup({
  headers: {
      'X-CSRF-TOKEN': $('meta[name="csrf_token"]').attr('content')
    }
  });

  var Alert = Swal.mixin({
    position: 'center'
  });

function saveAset(){
  var ivnSn = $('#ivnSn').val();
  var ivnMerk = $('#ivnMerk').val();
  var ivnProcessor = $('#ivnProcessor').val();
  var ivnRam = $('#ivnRam').val();
  var ivnStorage = $('#ivnStorage').val();
  var ivnOs = $('#ivnOs').val();
  var ivnUser = $('#ivnUser').val();
  var ivnDivisi = $('#ivnDivisi').val();
  var ivnTanggal = $('#ivnTanggal').val();
  var ivnKeterangan = $('#ivnKeterangan').val();
  $.ajax({
    type: 'POST',
    dataType: 'json',
    data: {
      "_token": "{{ csrf_token() }}", 
      ivnSn:ivnSn, 
      ivnMerk:ivnMerk, 
      ivnProcessor:ivnProcessor, 
      ivnRam:ivnRam, 
      ivnStorage:ivnStorage, 
      ivnOs:ivnOs, 
      ivnUser:ivnUser, 
      ivnDivisi:ivnDivisi, 
      ivnTanggal:ivnTanggal, 
      ivnKeterangan:ivnKeterangan
    },
    url: '/inven/store',
    success: function(response) {
      Alert.fire({
        type: 'success',
        title: 'Data Berhasil Disimpan.',
        showConfirmButton: true,
      });
        location.reload();
    },
    error: function(response){
      Alert.fire({
        type:  'warning',
        title: 'Data Belum Lengkap',
        text: 'Silakan periksa kembali',
        confirmButtonClass: "btn-success",
        confirmButtonText: "OK",
        showConfirmButton: true,
      });
    }
  })
}

$(document).ready(function(){
$('#dataInven').DataTable({
    "dom" : 'Bfrtip',
    "buttons" : [
      {
          extend: 'excel',
          text: 'EXPORT EXCEL',
          autoFilter: true,
          exportOptions: {
            columns:  [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]
          }               
      }   
    ],
    columnDefs:[
        {  
        "targets":[2, 3, 4, 5, 7, 9],  
        "visible": false  
      }
      ],
    "processing" : true,
    "serverSide" : true,
    "ordering" : false,
    "ajax" : {
      url: '/inven/json',
    },
    "columns" : [
      {data : 'ivnSn'},
      {data : 'ivnMerk'},
      {data : 'ivnProcessor'},
      {data : 'ivnRam'},
      {data : 'ivnStorage'},
      {data : 'ivnOs'},
      {data : 'ivnUser'},
      {data : 'ivnDivisi'},
      {data : null,
      render:function(data, type, row)
      {
        return moment(data['ivnTanggal']).format('DD MMM YYYY');
      }
      },
      {data : 'ivnKeterangan'},
      {data : 'detail',
      searchable: false},
      {data : 'action',
      searchable: false}
    ]
  });
});

$(document).ready(function() {
  $(document).on('click', '.detail', function(){
     var id = $(this).attr('id');
     $.ajax({  
                url:'/inven/singledata'+ id,
                type: 'POST',
                dataType:'json',
                data: {"_token": "{{ csrf_token() }}", id:id}, 
                success:function(response)  
                {  
                     $('#tesModal').modal('show'); 
                     $('.modal-body #ivnSn').text(response.ivnSn);
                     $('.modal-body #ivnMerk').text(response.ivnMerk);
                     $('.modal-body #ivnProcessor').text(response.ivnProcessor);
                     $('.modal-body #ivnRam').text(response.ivnRam);
                     $('.modal-body #ivnStorage').text(response.ivnStorage);
                     $('.modal-body #ivnOs').text(response.ivnOs);
                     $('.modal-body #ivnTanggal').text(response.ivnTanggal);
                     $('.modal-body #ivnKeterangan').text(response.ivnKeterangan);2
                     $('.modal-header #ivnUser').text(response.ivnUser);
                     $('.modal-header #ivnDivisi').text(response.ivnDivisi);  
                     $('.modal-header #ivnDate').text(response.created_at);  
                } 
      });
      });
    
      
});

// $(document).on('click', '.edit', function(){
//      var id = $(this).attr('id');
//      $('#form_result').html('');
//      $.ajax({
//       url :"/inven/"+id,
//       type : "GET",
//       dataType:"json",
//       success:function(data)
//       {
//        $('#first_name').val(data.result.first_name);
//        $('#last_name').val(data.result.last_name);
//        $('#hidden_id').val(id);
//        $('.modal-title').text('Edit Record');
//        $('#action_button').val('Edit');
//        $('#action').val('Edit');
//        $('#formModal').modal('show');
//       }
//      })
// });

var kode;

$(document).on('click', '.delete', function(){  
    kode = $(this).attr("id");
    $("#confirmModal").modal("show");

    $('#konfirmasi').click(function(){
     $.ajax({
      url:"/inven/"+kode,
      type: "DELETE",
      data: {  
        "_token": "{{ csrf_token() }}",
        kode:kode 
      },
      success:function(data)
      {
       setTimeout(function(){
         Alert.fire({
           type: 'success',
           title: 'Data Berhasil dihapus.',
           showConfirmButton: false,
           timer: 2000
           });
        $('#confirmModal').modal('hide');
        $('#dataInven').DataTable().ajax.reload();
       }, 1000);
      }
     })
    })
});
</script>
@endsection