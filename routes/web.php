<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'DashboardController@index');
Route::get('/inven', 'DashboardController@absen')->name('inven');
Route::get('/inven/json', 'DashboardController@invenjson');
Route::post('/inven/singledata{id}', 'DashboardController@show');
Route::post('/inven/store', 'DashboardController@store');
Route::delete('inven/{id}', 'DashboardController@destroy');
Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');