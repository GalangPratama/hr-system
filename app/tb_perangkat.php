<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class tb_perangkat extends Model
{
    protected $table = 'perangkat';
    protected $primaryKey = 'id';
    public $incrementing = false;
    protected $fillable = [
        'invSn', 'ivnMerk', 'ivnProcessor', 'invRam', 'ivnStorage', 'ivnOs', 'ivnUser', 'ivnDivisi', 'ivnTanggal', 'ivnKeterangan'
    ];
}