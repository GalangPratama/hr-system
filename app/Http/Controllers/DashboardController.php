<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\tb_perangkat;
use DataTables;
class DashboardController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('content.live-absen');
    }

    public function absen()
    {
        return view('content.absen');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'ivnSn' => 'required|unique:perangkat',
            'ivnMerk'=> 'required',
            'ivnProcessor'=> 'required',
            'ivnRam' => 'required',
            'ivnStorage'=> 'required',
            'ivnOs'=> 'required',
            'ivnUser'=> 'required',
            'ivnDivisi'=> 'required',
            'ivnTanggal'=> 'nullable',
            'ivnKeterangan'=> 'nullable'
        ]);
        
        $post = new tb_perangkat();
        $post->ivnSn = $request->ivnSn;
        $post->ivnMerk = $request->ivnMerk;
        $post->ivnProcessor = $request->ivnProcessor;
        $post->ivnRam = $request->ivnRam;
        $post->ivnStorage = $request->ivnStorage;
        $post->ivnOs = $request->ivnOs;
        $post->ivnUser = $request->ivnUser;
        $post->ivnDivisi = $request->ivnDivisi;
        $post->ivnTanggal = $request->ivnTanggal;
        $post->ivnKeterangan = $request->ivnKeterangan;
        $post->save();
   
        return response()->json($post);

        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = tb_perangkat::find($id);

        return response()->json($data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        tb_perangkat::find($id)->delete();
        return response()->json(['success'=>'Job deleted successfully.']);
    }
 
    public function invenjson(Request $request){
        $data = tb_perangkat::orderBy('id', 'desc')->get();
        return DataTables::of($data)
        ->addColumn('detail', function($data){
            $button = '<button type="button" name="detail" id="'. $data->id .'" class="detail btn btn-primary btn-sm">Detail</button>';
            return $button;
        })
        ->addColumn('action', function($data){
            $button = '<button type="button" name="edit" id="'. $data->id .'" class="edit btn btn-primary btn-sm">EDIT</button> <button type="button" name="delete" id="'. $data->id .'" class="delete btn btn-danger btn-sm">DELETE</button>';
            return $button;
        })
        ->rawColumns(['detail','action'])
        ->make(true);
        return response()->json($data);
    
    }

}