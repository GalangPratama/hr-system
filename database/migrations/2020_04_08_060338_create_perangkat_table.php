<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePerangkatTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('perangkat', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('ivnSn', 30)->unique();
            $table->string('ivnMerk', 20);
            $table->string('ivnProcessor', 10);
            $table->string('ivnRam', 10);
            $table->string('ivnStorage', 10);
            $table->string('ivnOs', 10);
            $table->string('ivnUser', 20);
            $table->string('ivnDivisi', 20);
            $table->date('ivnTanggal')->nullable();
            $table->text('ivnKeterangan')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('perangkat');
    }
}